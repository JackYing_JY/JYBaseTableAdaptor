//
//  JYBaseRegHeaderView.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseHeaderView.h"

@interface JYBaseHeaderView ()

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *subTitleLab;

@end

@implementation JYBaseHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
//        UIView *bgView = [[UIView alloc] init];
//        bgView.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.titleLab = [[UILabel alloc] initWithFrame:CGRectMake(16, 16, 0, 0)];
        self.titleLab.textAlignment = NSTextAlignmentCenter;
        self.titleLab.font = [UIFont boldSystemFontOfSize:22];
        [self.contentView addSubview:self.titleLab];
        
        self.subTitleLab = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLab.frame), CGRectGetMaxY(self.titleLab.frame) + 26, 0, 0)];
        self.subTitleLab.textAlignment = NSTextAlignmentCenter;
        self.subTitleLab.font = [UIFont systemFontOfSize:18];
        self.subTitleLab.textColor = [UIColor grayColor];
        self.subTitleLab.numberOfLines = 0;
        [self.contentView addSubview:self.subTitleLab];
    }
    return self;
}

- (void)setHeaderModel:(JYBaseHeaderModel *)headerModel {
    
    if (_headerModel != headerModel) {
        _headerModel = headerModel;
    }
    self.titleLab.text = _headerModel.title;
    self.subTitleLab.text = _headerModel.subTitle;

    [self.titleLab sizeToFit];
    [self.subTitleLab sizeToFit];
    self.contentView.backgroundColor = headerModel.backgroundColor;
}

@end
