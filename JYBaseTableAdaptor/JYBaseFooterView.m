//
//  JYBaseRegFooterView.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseFooterView.h"

@interface JYBaseFooterView () {
    UIButton *nextBtn;
}

@end

@implementation JYBaseFooterView


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        nextBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [nextBtn addTarget:self action:@selector(nextAction:) forControlEvents:(UIControlEventTouchUpInside)];
        nextBtn.layer.cornerRadius = 5;
        nextBtn.layer.masksToBounds = YES;
        [self addSubview:nextBtn];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = frame.size.height * 2/3 - 20;
    frame.size.width -= 32;
    frame.origin.y = frame.size.height / 6 + 20;
    frame.origin.x += 16;
    nextBtn.frame = frame;
}

- (void)setModel:(JYBaseFooterModel *)model {
    
    if (_model != model) {
        _model = model;
    }
    [nextBtn setTitle:model.title forState:(UIControlStateNormal)];
    [nextBtn setBackgroundColor:model.backgroundColor];
}

- (void)nextAction:(UIButton *)sender {
    if (self.nextAction) {
        self.nextAction(self.model, @"next");
    }
}

@end
