//
//  UITableViewCell+JYHierarchy.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/19/19.
//  Copyright © 2019 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableViewCell (JYHierarchy)

- (void)filteroutCellSeparatorView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
