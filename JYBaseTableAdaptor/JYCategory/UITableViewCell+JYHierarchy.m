//
//  UITableViewCell+JYHierarchy.m
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/19/19.
//  Copyright © 2019 JackYing. All rights reserved.
//

#import "UITableViewCell+JYHierarchy.h"

@implementation UITableViewCell (JYHierarchy)

- (void)filteroutCellSeparatorView:(UIView *)view {
    if (view && ![view isKindOfClass:[NSClassFromString(@"_UITableViewCellSeparatorView") class]]) {
        [super addSubview:view];
    }
}

@end
