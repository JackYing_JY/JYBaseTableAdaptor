//
//  JYTableViewHeaderFooterView.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/8/2.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseHeaderFooterView.h"

@interface JYBaseHeaderFooterView () {
    
}

@property (nonatomic, strong, nullable) UILabel *textLabel;
@property (nonatomic, strong, nullable) UILabel *detailTextLabel;

@end

@implementation JYBaseHeaderFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.textLabel = [[UILabel alloc] init];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.font = [UIFont boldSystemFontOfSize:22];
        [self addSubview:self.textLabel];
        
        self.detailTextLabel = [[UILabel alloc] init];
        self.detailTextLabel.textAlignment = NSTextAlignmentCenter;
        self.detailTextLabel.font = [UIFont systemFontOfSize:18];
        self.detailTextLabel.textColor = [UIColor grayColor];
        [self addSubview:self.detailTextLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.frame;
    CGFloat height = CGRectGetHeight(frame);
    CGFloat width = CGRectGetWidth(frame);
    
    if (self.model.title && self.model.subTitle) {
        height /= 2;
        self.textLabel.frame = CGRectMake(0, 0, width, height);
        self.detailTextLabel.frame = CGRectMake(0, height, width, height);
    } else if (self.model.title && !self.model.subTitle) {
        
        self.textLabel.frame = CGRectMake(0, 0, width, height);
    } else if (!self.model.title && self.model.subTitle) {
        
        self.detailTextLabel.frame = CGRectMake(0, 0, width, height);
    }
}

- (void)setModel:(JYBaseHeaderFooterModel *)model {
    
    if (_model != model) {
        _model = model;
    }
    self.textLabel.text = _model.title;
    self.detailTextLabel.text = _model.subTitle;  
}


@end
