//
//  JYBaseRegModel.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseCellModel.h"

@implementation JYBaseCellModel
@synthesize estimatedHeight = _estimatedHeight;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.need = NO;
        self.edgeInsets = UIEdgeInsetsMake(1, 20, 1, 20);
        self.heightSource = JYCellHeightSource_Low;
    }
    return self;
}

- (void)dealloc {
#ifdef DEBUG
    NSLog(@"[%@ --> %p, %@]", NSStringFromClass([self class]), self, @"dealloc");
#endif
}

+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title {
    
    JYBaseCellModel *model = [[self alloc] init];
    model.itemKey = (itemKey == nil ? @"null" : itemKey);
    model.title = (title == nil ? @"null" : title);
    return model;
}

+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title content:(NSString *)content {
    JYBaseCellModel *model = [self modelWithItemKey:itemKey title:title];
    model.content = content;
    return model;
}

- (NSString *)description {
    NSString *description = [NSString stringWithFormat:@"<%p>, title:%@ itemKey:%@ contentID:%@ content:%@", self, _title, _itemKey, _contentID, _content];
    
    return description;
}

- (NSUInteger)hash {
    NSString *stringToHash = [NSString stringWithFormat:@"%@:%@", _itemKey, _title];
    return [stringToHash hash];
}

- (BOOL)isEqual:(JYBaseCellModel *)object {
    if ([object isKindOfClass:[self class]] == NO) return NO;
    
    if ([self.itemKey isEqualToString:object.itemKey]) {
        return YES;
    }
    return [super isEqual:object];
}

- (CGFloat)estimatedHeight {
    if ((self.heightSource & JYCellHeightSource_High) == JYCellHeightSource_High) {
        return _estimatedHeight;
    } else {
        return _estimatedHeight < 1 ? 44 : _estimatedHeight;
    }
}

- (void)setEstimatedHeight:(CGFloat)estimatedHeight {
    self.heightSource = JYCellHeightSource_High;
    _estimatedHeight = estimatedHeight;
}


- (BOOL)verifyContent {
    if (self.isNeed
        && (!self.content && !self.contentID)) {
        return NO;
    }
    return YES;
}

@end
