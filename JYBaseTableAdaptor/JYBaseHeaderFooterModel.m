//
//  JYBaseRegHeaderModel.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseHeaderFooterModel.h"
@interface JYBaseHeaderFooterModel () {
    BOOL isFooter;
}

@end

@implementation JYBaseHeaderFooterModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.estimatedHeight = 120;
    }
    return self;
}

+ (instancetype)modelHeaderWithTitle:(NSString *)title subTitle:(NSString *)subTitle {
    JYBaseHeaderFooterModel *modle = [[self alloc] init];
    modle.title = title;
    modle.subTitle = subTitle;
    return modle;
}

+ (instancetype)modelFooterWithTitle:(NSString *)title {
    JYBaseHeaderFooterModel *modle = [self modelHeaderWithTitle:title subTitle:nil];
    modle.estimatedHeight = 44 * 2;
    modle->isFooter = YES;
    return modle;
}


- (CGFloat)estimatedHeight {
    
    if (isFooter) {
        if (self.title) {
            // 脚视图有标题时，默认返回
            return _estimatedHeight;
        }
        // 如果不需要脚视图
        return 0.0001;
    }
    
    if ((!self.title && !self.subTitle)) {
        return 0.0001;
    }
    return _estimatedHeight;
}

@end
