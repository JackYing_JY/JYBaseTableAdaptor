//
//  JYBaseRegTableViewCell.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/4.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseInputCell.h"
#import "JYBaseInputModel.h"

@implementation JYBaseInputCell
@dynamic contentV;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.contentV removeFromSuperview];
    CGRect frame = self.bounds;
    frame.origin.x = CGRectGetMaxX(self.titleLab.frame);
    frame.size.width = CGRectGetWidth(self.bounds) - CGRectGetMaxX(self.titleLab.frame) - 10;
    JYBaseInputView *inputView = [[JYBaseInputView alloc] initWithFrame:frame];
    inputView.delegate = self;
    self.contentV = inputView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentV removeFromSuperview];
        CGRect frame = self.bounds;
        frame.origin.x = CGRectGetMaxX(self.titleLab.frame);
        frame.size.width = CGRectGetWidth(self.bounds) - CGRectGetMaxX(self.titleLab.frame) - 10;
        JYBaseInputView *inputView = [[JYBaseInputView alloc] initWithFrame:frame];
        inputView.delegate = self;
        self.contentV = inputView;
    }
    return self;
}

- (void)setModel:(JYBaseInputModel *)model {
    [super setModel:model];
    [self.contentV setContent:model];
    if ([self.contentV respondsToSelector:@selector(setPlaceholder:)]) {    
        [self.contentV setPlaceholder:model.placeholder];
    }
    if ([self.contentV respondsToSelector:@selector(setEditable:)]) {
        [self.contentV setEditable:model.changeable];
    }
}


///-------------------------------------------cell.contentV回调-----------------------------------------------
#pragma mark - <JYInfoInputViewDelegate> 信息输入视图回调
- (void)inputViewDidBeginInputView:(UIView<JYAdaptorInputViewProtocol> *)inputView {
    
}

- (void)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView didFinishInput:(id)content {
    self.model.content = content;
}
@end
