//
//  JYBaseRegHeaderModel.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYBaseHeaderFooterModel : NSObject

@property (nonatomic, copy) NSString *itemKey;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;
@property (nonatomic, assign) CGFloat estimatedHeight;
@property (nonatomic, strong) UIColor *backgroundColor;

+ (instancetype)modelHeaderWithTitle:(NSString *)title subTitle:(NSString *)subTitle;
+ (instancetype)modelFooterWithTitle:(NSString *)title;

@end
