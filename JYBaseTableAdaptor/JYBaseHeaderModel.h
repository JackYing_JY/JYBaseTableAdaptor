//
//  JYBaseHeaderModel.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/28/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#import "JYBaseHeaderFooterModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface JYBaseHeaderModel : JYBaseHeaderFooterModel

@end

NS_ASSUME_NONNULL_END
