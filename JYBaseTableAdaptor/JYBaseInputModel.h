//
//  JYBaseRegModel.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/4.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseCellModel.h"

@interface JYBaseInputModel : JYBaseCellModel

@property (nonatomic, copy) NSString *placeholder;
// 可支持的正则表达式
@property (nonatomic, copy) NSString *regEx;
@property(nonatomic) UIKeyboardType keyboardType;

+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title placeholder:(NSString *)contentPlaceholder;


/// 控制对应视图是否支持输入或选择，
/// 部分内容不可编辑和重新选择
@property (nonatomic, assign, getter=isChangeabel) BOOL changeable;

/// 输入内容是否为空
- (BOOL)verifyContentLength;
/// 验证内容包括，输入内容是否为空、输入内容是否与正则匹配，
/// 子类进行重写该方法
- (BOOL)verifyContent;

@end
