//
//  JYTableAdaptorProtocol.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/9.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JYBaseCellModel, JYTableBaseAdaptor, JYBaseActionModel;

typedef void(^CellSelectedAction)(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *cellModel);
typedef id(^CellAccessoryAction)(JYBaseActionModel *action, JYBaseCellModel *cellModel);

typedef NSArray<UITableViewRowAction *> *(^CellEditAction)(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model);
typedef BOOL (^CellEditable)(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model);

typedef void(^CellMoveAction)(NSIndexPath *sourceIndexPath, NSIndexPath *destinationIndexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model);
typedef BOOL (^CellMoveable)(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model);



@protocol JYAdaptorAdaptorProtocol <NSObject>

@required
#pragma mark - TableViewCell
- (void)regsiterCell;
/// 将某一种数据模型与指定cell进行绑定, one cell.class Associate one model.class
- (void)regsiterCell:(Class)cellClass forModelClass:(Class)mClazz;
- (void)regsiterCellNib:(Class)cellNibClass forModelClass:(Class)mClazz;


/// 将某 多种模型 与 一种cell 进行绑定，对cell进行重用
- (void)regsiterCell:(Class)cellClass forModelClass:(Class)modelClazzD extid:(NSString *)extid;
- (void)regsiterCellNib:(Class)cellNibClass forModelClass:(Class)modelClazz extid:(NSString *)extid;


#pragma mark - 点击事件
/// 点击cell动作
@property (nonatomic, copy) CellSelectedAction cellSelectedAction;
/// 点击cell内子视图动作
@property (nonatomic, copy) CellAccessoryAction cellAccessoryAction;



@optional
#pragma mark - 分组头尾视图
- (void)regsiterSectionHFView;
/// 将某一种数据模型与指定HeaderFooter进行绑定
- (void)regsiterSectionHFView:(Class)hfClass forModelClass:(Class)mClazz;

#pragma mark - 左滑编辑
/// cell高度较低时，优先显示图标
@property (nonatomic, copy) CellEditAction cellEditAction;
@property (nonatomic, copy) CellEditable cellEditable;
/// 默认含两个2个
@property (nonatomic, copy) NSArray<UIImage *> *cellEditIcon;
/// 记录当前编辑的cell
@property (nonatomic, strong, readonly) NSIndexPath* cellEditingIndexPath;


#pragma mark - 移动
@property (nonatomic, copy) CellMoveAction cellMoveAction;
@property (nonatomic, copy) CellMoveable cellMoveable;

@end
