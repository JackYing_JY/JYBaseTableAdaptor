//
//  JYActionModel.h
//  YBSYBank-iPhone
//
//  Created by 应明顺 on 1/10/19.
//  Copyright © 2019 CSII. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 选中一个
extern NSString * const kJYActionIDSelected;
/// 选择更多
extern NSString * const kJYActionIDMore;
/// 触发某个交易
extern NSString * const kJYActionIDToggle;

@interface JYBaseActionModel : NSObject

@property (nonatomic, strong) NSString *actionID;
@property (nonatomic, strong) NSString *actionName;
/// 通过URL进行解析
@property (nonatomic, strong) NSString *URLStr;
/// 携带任意内容
@property (nonatomic, strong) id content;

@end

NS_ASSUME_NONNULL_END
