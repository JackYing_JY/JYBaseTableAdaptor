//
//  JYBaseRegGroupModel.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/8/2.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseGroupModel.h"

@interface JYBaseGroupModel ()
@property (nonatomic, strong) NSMutableArray *rowModelArr;
@end

@implementation JYBaseGroupModel
@synthesize rowModelArr = _rowModelArr;

- (instancetype)init {
    if (self = [super init]) {
        self.rowModelArr = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
#ifdef DEBUG
    NSLog(@"[%@ --> %p, %@]", NSStringFromClass([self class]), self, @"dealloc");
#endif
}

+ (JYBaseGroupModel *)groupModelWithRowModelArr:(NSArray<JYBaseCellModel *> *)modelArr headerModel:(JYBaseHeaderModel *)headerModel footerModel:(JYBaseFooterModel *)footerModel {
    JYBaseGroupModel *model = [[self alloc] init];
    [model.rowModelArr addObjectsFromArray:modelArr];
    model.headerModel = headerModel;
    model.footerModel = footerModel;
    return model;
}

+ (JYBaseGroupModel *)groupModelWithRowModelArr:(NSArray <JYBaseCellModel *> *)rowModelArr {
    return [self groupModelWithRowModelArr:rowModelArr headerModel:nil footerModel:nil];
}

- (JYBaseGroupModel *)appendRowModel:(JYBaseCellModel *)rowModel{
    [self.rowModelArr addObject:rowModel];
    return self;
}

- (JYBaseGroupModel *)addRowModelsFromArray:(NSArray<JYBaseCellModel *> *)rowModelArr {
    [self.rowModelArr addObjectsFromArray:rowModelArr];
    return self;
}

- (JYBaseGroupModel *)insertRowModel:(JYBaseCellModel *)rowModel atIndex:(NSInteger)index {
    if (rowModel && (index == 0 || index < self.rowCount)) {
        [self.rowModelArr insertObject:rowModel atIndex:index];
    }
    return self;
}

- (JYBaseGroupModel *)insertRowModels:(NSArray<JYBaseCellModel *> *)rowModels atIndexSet:(NSIndexSet *)indexSet {
    if (rowModels && [indexSet indexLessThanIndex: self.rowCount]) {
        [self.rowModelArr insertObjects:rowModels atIndexes:[[NSIndexSet alloc] initWithIndexSet:indexSet]];
    }
    return self;
}

- (JYBaseCellModel *)deleteRowModelAtIndex:(NSInteger)index {
    JYBaseCellModel *deletedModel;
    if (index < self.rowCount) {
        deletedModel = [self.rowModelArr objectAtIndex:index];
        [self.rowModelArr removeObjectAtIndex:index];
    }
    return deletedModel;
}

- (void)deleteRowModel:(JYBaseCellModel *)mode {
    if (mode) {
        [self.rowModelArr removeObject:mode];
    }
}

- (NSArray <JYBaseCellModel *> *)deleteRowModelsAtIndexSet:(NSIndexSet *)indexSet {
    NSArray *deletedModels;
    if ([indexSet indexLessThanOrEqualToIndex:self.rowCount]) {
        deletedModels = [self.rowModelArr objectsAtIndexes:indexSet];
        [self.rowModelArr removeObjectsAtIndexes:indexSet];
    }
    return deletedModels;
}

- (NSArray<JYBaseCellModel *> *)deleteAllRowModels {
    NSArray *tmp = [self.rowModelArr copy];
    [self.rowModelArr removeAllObjects];
    return tmp;
}


- (NSInteger)rowCount {
    return self.rowModelArr.count;
}

- (NSArray *)getRowModelArr{
    return [self.rowModelArr copy];
}

- (JYBaseCellModel *)modelAtIndex:(NSInteger)index {
    if (index > self.rowModelArr.count - 1) {
        return nil;
    };
    return [self.rowModelArr objectAtIndex:index];
}

@end
