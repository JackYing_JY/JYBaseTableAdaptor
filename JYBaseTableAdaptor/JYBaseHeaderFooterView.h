//
//  JYTableViewHeaderFooterView.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/8/2.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYBaseHeaderFooterModel.h"

@interface JYBaseHeaderFooterView : UIView

@property (nonatomic, strong, nonnull) JYBaseHeaderFooterModel *model;

@property (nonatomic, readonly, strong, nullable) UILabel *textLabel;
@property (nonatomic, readonly, strong, nullable) UILabel *detailTextLabel;

@end
