//
//  JYTableBaseAdaptor.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/4.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JYAdaptorInputViewProtocol.h"
#import "JYAdaptorAdaptorProtocol.h"
#import "JYBaseGroupModel.h"


@class JYTableBaseAdaptor;

extern NSInteger const kTagBaseRegContentView;
extern NSInteger const kBaseSection;


/**
 Adaptor
 */
@interface JYTableBaseAdaptor : NSObject <UITableViewDataSource, UITableViewDelegate, JYAdaptorAdaptorProtocol> {
    @protected
    NSMapTable <id, NSNumber *> *mapTabel;
}

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *key;

@property (nonatomic, weak, readonly) UITableView *tableView;

/**
 初始化Adaptor

 @param tableView 需要展示的tableView，tv会被弱引用，用于注册cell
 @param title 标识该tv
 @param key 标识该tv
 */
+ (instancetype)tableAdaptorWithTableView:(UITableView *)tableView title:(NSString *)title key:(NSString *)key;

/// 按分组进行数据管理
@property (nonatomic, strong, readonly) NSMutableArray <JYBaseGroupModel *> *dataArr;
- (void)setDataArr:(NSMutableArray<JYBaseGroupModel *> *)dataArr;

/// 表格头，放温馨提示之类的辅助信息
@property (nonatomic, strong) JYBaseHeaderFooterModel *tableHeaderModel;
/// 表格尾，放温馨提示之类的辅助信息
@property (nonatomic, strong) JYBaseHeaderFooterModel *tableFooterModel;

// 共用方法
- (JYBaseCellModel *)modelWithItemKey:(NSString *)itemKey;
- (JYBaseCellModel *)modelWithItemKey:(NSString *)itemKey fromIndex:(NSInteger)index;
- (JYBaseCellModel *)modelWithIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)indexPathForInputView:(UIView *)inputView;
- (NSIndexPath *)indexPathForModel:(JYBaseCellModel *)model;
- (NSIndexPath *)indexPathForModelItemKey:(NSString *)itemKey;
/// 从指定位置开始向后查找
- (NSIndexPath *)indexPathForModelItemKey:(NSString *)itemKey fromIndex:(NSInteger)index;
- (NSInteger)tagForInputViewWithIndexPath:(NSIndexPath *)indexPath;
- (UIView *)inputViewForIndexPath:(NSIndexPath *)indexPath;

/// 在DataSource中查找某一类model的第一个的indexpath
- (NSIndexPath *)indexOfLeadModelForClass:(Class)modelClass;


#pragma mark - 常规操作
/// 下一步，分组尾的动作
@property (nonatomic, copy) void(^nextAction)(JYBaseHeaderFooterModel *footerModel, id action);
@property (nonatomic, copy) CellSelectedAction cellSelectedAction;
@property (nonatomic, copy) CellAccessoryAction cellAccessoryAction;

#pragma mark - 左滑相关操作
/// cell高度较低时，优先显示图标
@property (nonatomic, copy) CellEditAction cellEditAction;
@property (nonatomic, copy) CellEditable cellEditable;
@property (nonatomic, copy) NSArray<UIImage *> *cellEditIcon;
/// 记录当前编辑的cell
@property (nonatomic, strong, readonly) NSIndexPath* cellEditingIndexPath;

// 移动
@property (nonatomic, copy) CellMoveAction cellMoveAction;
@property (nonatomic, copy) CellMoveable cellMoveable;

/// 拼接一组数据
- (JYTableBaseAdaptor *)appendGroup:(JYBaseGroupModel *)groupModel;
/// 拼接多组数据
- (JYTableBaseAdaptor *)appendGroupList:(NSArray <JYBaseGroupModel *> *)groups;
/// 插入一组数据
- (JYTableBaseAdaptor *)insertGroup:(JYBaseGroupModel *)groupModel atIndex:(NSInteger)index;

/// 在某个组删除
- (JYBaseGroupModel *)deleteGroupAtIndex:(NSInteger)index;
- (NSArray *)deleteAllGroup;


/// 查找某组数据
- (JYBaseGroupModel *)groupAtIndex:(NSInteger)index;
/// 通过key查找某组数据
- (JYBaseGroupModel *)groupForKey:(NSString *)key;


@end
