//
//  JYTableBaseAdaptor+EditDecorate.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 12/3/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#import "JYTableBaseAdaptor.h"

@interface JYTableBaseAdaptor (EditDecorate)

/// 默认只支持两个按钮
- (void)configSwipeButtons;

@end
