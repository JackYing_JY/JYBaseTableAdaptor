//
//  UIView+JYTabelAdaptor.h
//  YBSYBank-iPhone
//
//  Created by 应明顺 on 2/14/19.
//  Copyright © 2019 CSII. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYTableBaseAdaptor.h"
#import "JYAdaptorContainerProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@protocol JYUIViewAdaptorProtocol <JYAdaptorContainerProtocol>


@optional

/// 表格内容顶部到表格顶部的间隙，无默认值
@property (nonatomic, assign) CGFloat topMargin;

@end


@interface UIView (JYTabelAdaptor) <JYUIViewAdaptorProtocol>

- (void)configTableAdaptor;

/// 配置初始化数据，当使用网络请求时根据所需可以选择性重写
- (NSArray<JYBaseGroupModel *> *)configTableViewDataSource;

/// 对下一页tableview上的下一页按钮设置事件
- (void)nextAction:(JYBaseHeaderFooterModel *)footerModel;

/// 配置cell的点击事件和cell的附属事件
- (void)configAdaptorAction;

- (BOOL)verifyDataSourceContent;


- (void)placeTableViewHeaderFooter;
@end

NS_ASSUME_NONNULL_END
