//
//  UIView+JYTabelAdaptor.m
//  YBSYBank-iPhone
//
//  Created by 应明顺 on 2/14/19.
//  Copyright © 2019 CSII. All rights reserved.
//

#import "UIView+JYTabelAdaptor.h"
#import "JYBaseHeaderFooterView.h"
#import <objc/runtime.h>


static const char * tableViewKey = "tableViewKey";
static const char * adaptorKey = "adaptor";
static const char * dataArrKey = "dataArrKey";
static const char * topMarginKey = "topMarginKey";


@implementation UIView (JYTabelAdaptor)


- (void)configTableAdaptor {
    // self.topMargin = 16; // 不应该再传
    [self addSubview:self.tableView];
    self.tableView.dataSource = self.adaptor;
    self.tableView.delegate = self.adaptor;
    
    [self configAdaptorAction];
}

- (NSArray *)configTableViewDataSource {
    return nil;
}

- (void)configAdaptorAction {
    
}

- (void)placeTableViewHeaderFooter {
    
    CGRect frame = self.tableView.bounds;
    if (self.adaptor.tableHeaderModel) {
        frame.size.height = self.adaptor.tableHeaderModel.estimatedHeight;
        JYBaseHeaderFooterView *header = [[JYBaseHeaderFooterView alloc] initWithFrame:frame];
        header.model = self.adaptor.tableHeaderModel;
        self.tableView.tableHeaderView = header;
    }
    
    if (self.adaptor.tableFooterModel) {
        frame.size.height = self.adaptor.tableFooterModel.estimatedHeight;
        JYBaseHeaderFooterView *footer = [[JYBaseHeaderFooterView alloc] initWithFrame:frame];
        footer.model = self.adaptor.tableFooterModel;
        self.tableView.tableFooterView = footer;
    }
}

- (void)nextAction:(JYBaseHeaderFooterModel *)footerModel {
    [self.tableView endEditing:YES];
    NSLog(@"%s", __func__);
}

- (BOOL)verifyDataSourceContent {
    
    // Mark: 按实际需求进行验证
    BOOL verify = YES;
    for (JYBaseGroupModel* section in self.adaptor.dataArr) {
        for (JYBaseCellModel *m in section.rowModelArr) {
            if (![m verifyContent]) {
                NSLog(@"%@为必填项", m.title);
                return NO;
            } else {
                NSLog(@"title:%-10@---> content:%-10@", m.itemKey, m.content);
            }
        }
    }
    
    return verify;
}

#pragma mark - Associated

- (CGFloat)topMargin {
    NSNumber *topMargin = objc_getAssociatedObject(self, topMarginKey);
    return [topMargin floatValue];
}

- (void)setTopMargin:(CGFloat)topMargin {
    objc_setAssociatedObject(self, topMarginKey, @(topMargin), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.tableView.contentInset = UIEdgeInsetsMake(topMargin, 0, 0, 0); // 设置时更新
}

- (UITableView *)tableView {
    UITableView *_tableView = objc_getAssociatedObject(self, tableViewKey);
    if (!_tableView) {
        
        // 适配导航栏
        CGRect frame = self.bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:(UITableViewStyleGrouped)];
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.contentInset = UIEdgeInsetsMake(self.topMargin, 0, 0, 0); // +16目的是顶部到菜单底部有空隙
        
        objc_setAssociatedObject(self, tableViewKey, _tableView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _tableView;
}

- (void)setTableView:(UITableView *)tableView {
    objc_setAssociatedObject(self, tableViewKey, tableView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableArray *)dataArr {
    NSMutableArray *_dataArr = objc_getAssociatedObject(self, dataArrKey);
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
        [_dataArr addObjectsFromArray:[self configTableViewDataSource]];
        
        objc_setAssociatedObject(self, dataArrKey, _dataArr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _dataArr;
}

- (void)setDataArr:(NSMutableArray *)dataArr {
    objc_setAssociatedObject(self, dataArrKey, dataArr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (JYTableBaseAdaptor *)adaptor {
    JYTableBaseAdaptor *_adaptor = objc_getAssociatedObject(self, adaptorKey);
    if (!_adaptor) {
        _adaptor = [JYTableBaseAdaptor
                    tableAdaptorWithTableView:self.tableView title:@"请重新设置适配器" key:@"giveNewKey"];
        _adaptor.dataArr = self.dataArr;
        //_adaptor.tableHeaderModel = [JYBaseHeaderFooterModel modelHeaderWithTitle:@"表头" subTitle:@"默认样式"];
        //_adaptor.tableFooterModel = [JYBaseHeaderFooterModel modelFooterWithTitle:@"表尾"];
        
        objc_setAssociatedObject(self, adaptorKey, _adaptor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _adaptor;
}

- (void)setAdaptor:(JYTableBaseAdaptor *)adaptor {
    objc_setAssociatedObject(self, adaptorKey, adaptor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
