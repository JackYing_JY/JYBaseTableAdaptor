//
//  UIViewController+JYTabelAdaptor.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/27/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYTableBaseAdaptor.h"
#import "JYAdaptorContainerProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@protocol JYUIViewControllerAdaptorProtocol <JYAdaptorContainerProtocol>


@optional

/// 表格内容顶部到表格顶部的间隙，无默认值
@property (nonatomic, assign) CGFloat topMargin;

@end





@interface UIViewController (JYTabelAdaptor) <JYUIViewControllerAdaptorProtocol>


- (void)configTableAdaptor;

/// 配置初始化数据，当使用网络请求时根据所需可以选择性重写
- (NSArray<JYBaseGroupModel *> *)configTableViewDataSource;

/// 对下一页tableview上的下一页按钮设置事件
- (void)nextAction:(JYBaseHeaderFooterModel *)footerModel;

/// 配置cell的点击事件和cell的附属事件
- (void)configAdaptorAction;

- (BOOL)verifyDataSourceContent;


- (void)placeTableViewHeaderFooter;

@end




NS_ASSUME_NONNULL_END
