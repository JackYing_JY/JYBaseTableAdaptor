//
//  JYBaseRegModel.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, JYCellHeightSourceOptions) {
    JYCellHeightSource_Low = 1 << 0,
    JYCellHeightSource_Medium = 1 << 1,
    JYCellHeightSource_High = 1 << 2,
};

@interface JYBaseCellModel : NSObject

/// 在 多model 对 一cell 时需要额外添加一个标识，
/// 需要与注册时保持一致
@property (nonatomic, copy) NSString *extid;

@property (nonatomic, copy) NSString *title;
/**
 与title对应，即将title转成英文，与服务端字段对应
 */
@property (nonatomic, copy) NSString *itemKey;
/**
 用户获取录入信息可能存在itemID与content中的一个中
 */
@property (nonatomic, copy) NSNumber *contentID;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSIndexPath *indexPath;

/**
 默认NO，带输入框的默认是YES
 */
@property (nonatomic, assign, getter=isNeed) BOOL need;

/**
 预估高度
 */
@property (nonatomic, assign) CGFloat estimatedHeight;
@property (nonatomic, assign) JYCellHeightSourceOptions heightSource;

/**
 1. 更新方式一
 内容可能通过网络请求后进行了更新
 针对内容是列表类型的cell，对该属性设YES，随后对指定indexPaths进行刷新
 */
@property (nonatomic, assign) BOOL contentListUpdated;

// 内容框对齐方式
@property (nonatomic, assign) NSTextAlignment contentAlignment;
// 标题宽度自适应
@property (nonatomic, assign) BOOL titleSizeFit;

@property (nonatomic, assign) UIEdgeInsets edgeInsets;



+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title;
+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title content:(NSString *)content;



/**
 子类进行重写该方法
 验证内容包括，输入内容是否为空、输入内容是否与正则匹配，

 @return 返回验证结果
 */
- (BOOL)verifyContent;


@end
