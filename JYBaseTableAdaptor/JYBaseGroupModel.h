//
//  JYBaseRegGroupModel.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/8/2.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JYBaseCellModel.h"
#import "JYBaseHeaderModel.h"
#import "JYBaseFooterModel.h"

/// 某一组管理，对NSArray做封装，提供title和groupKey
@interface JYBaseGroupModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *groupKey;
@property (nonatomic, strong) JYBaseHeaderModel *headerModel;
@property (nonatomic, strong) JYBaseFooterModel *footerModel;
@property (nonatomic, assign, readonly) NSInteger rowCount;
@property (nonatomic, strong, readonly) NSMutableArray *rowModelArr;


+ (JYBaseGroupModel *)groupModelWithRowModelArr:(NSArray <JYBaseCellModel *> *)rowModelArr;
+ (JYBaseGroupModel *)groupModelWithRowModelArr:(NSArray <JYBaseCellModel *> *)rowModelArr headerModel:(JYBaseHeaderModel *)headerModel footerModel:(JYBaseFooterModel *)footerModel;


- (JYBaseGroupModel *)appendRowModel:(JYBaseCellModel *)rowModel;
- (JYBaseGroupModel *)addRowModelsFromArray:(NSArray <JYBaseCellModel *> *)rowModelArr;

/// 在某个分组内插入数据
- (JYBaseGroupModel *)insertRowModel:(JYBaseCellModel *)rowModel atIndex:(NSInteger)index;
/// 在某个分组内插入数据列表
- (JYBaseGroupModel *)insertRowModels:(NSArray <JYBaseCellModel *> *)rowModels atIndexSet:(NSIndexSet *)indexSet;

/// 在某个分组内删除数据
- (JYBaseCellModel *)deleteRowModelAtIndex:(NSInteger)index;
/// 在某个分组内删除数据
- (void)deleteRowModel:(JYBaseCellModel *)mode;
/// 在某个分组内删除数据列表
- (NSArray <JYBaseCellModel *> *)deleteRowModelsAtIndexSet:(NSIndexSet *)indexSet;
/// 移除所有
- (NSArray <JYBaseCellModel *> *)deleteAllRowModels;


- (NSArray *)getRowModelArr;
- (JYBaseCellModel *)modelAtIndex:(NSInteger)index;

@end
