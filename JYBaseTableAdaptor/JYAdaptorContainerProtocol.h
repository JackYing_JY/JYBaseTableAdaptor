//
//  JYAdaptorContainerProtocol.h
//  YBSYBank-iPhone
//
//  Created by 应明顺 on 4/25/19.
//  Copyright © 2019 CSII. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JYAdaptorContainerProtocol <NSObject>


@required

@property (nonatomic, strong) JYTableBaseAdaptor *adaptor; // 必须对adaptor进行持有，否则tableview在使用后adaptor立马会被销毁
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

NS_ASSUME_NONNULL_END
