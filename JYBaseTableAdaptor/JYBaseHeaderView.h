//
//  JYBaseRegHeaderView.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYBaseHeaderModel.h"

@interface JYBaseHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) JYBaseHeaderModel *headerModel;

@property (nonatomic, strong, readonly) UILabel *titleLab;
@property (nonatomic, strong, readonly) UILabel *subTitleLab;

@end
