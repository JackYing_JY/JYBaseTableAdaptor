//
//  JYInfoInputView.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYAdaptorInputViewProtocol.h"
#import "JYVerifyRegEx.h"

/**
 主要（默认）的录入控件为UITextField，可以
 */
@interface JYBaseInputView : UIView <UITextFieldDelegate, JYAdaptorInputViewProtocol>

@property (nonatomic, weak)     id<JYAdaptorInputViewDelegate> delegate;
@property(nonatomic)            UIEdgeInsets textEdgeInsets;

@property (nonatomic, strong)   UITextField *textField;

/// 可支持的正则表达式
@property (nonatomic, copy)     NSString *regEx;
@property (nonatomic, copy)     NSString *placeholder;
@property (nonatomic, copy)     NSString *text;

@property (nonatomic)           UIKeyboardType keyboardType;
@property (nonatomic, strong)   UIView *rightView;
@property (nonatomic)           UITextFieldViewMode clearButtonMode;
@property (nonatomic)           BOOL clearsOnBeginEditing;
@property (nonatomic, assign)   BOOL editable;


/**
 添加多个代理
 
 1. 有多个代理时，最后一个代理最先生效，不同代理可显示协议的不同内容
 2. 多个代理中仅最后一个可以控制inputview的是否可输入的状态
 3. 多个代理时，delegate始终保持在代理数组中的第一位，也就是最后被调用
 4. 存在多个代理时，设置delegate时，新设置的delegate将插入队首位置
*/
- (void)addDelegate:(id<JYAdaptorInputViewDelegate>)delegate;

@end
