//
//  JYBaseRegFooterView.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYBaseFooterModel.h"

/**
 占用footer高度的一半，垂直居中
 */
@interface JYBaseFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong) JYBaseFooterModel *model;
@property (nonatomic, copy) void(^nextAction)(JYBaseHeaderFooterModel *model, id action);




@end
