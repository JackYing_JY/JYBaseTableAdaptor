//
//  JYBaseTableAdaptor.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/21/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#ifndef JYBaseTableAdaptor_h
#define JYBaseTableAdaptor_h


#import "JYTableBaseAdaptor.h"
#import "UIViewController+JYTabelAdaptor.h"
#import "UIView+JYTabelAdaptor.h"
#import "JYAdaptorAdaptorProtocol.h"

#import "JYBaseTableViewCell.h"
#import "JYBaseInputCell.h"
#import "JYAdaptorInputViewProtocol.h"

#import "JYBaseActionModel.h"
#import "JYBaseCellModel.h"
#import "JYBaseInputModel.h"
#import "JYBaseGroupModel.h"


#endif /* JYBaseTableAdaptor_h */

