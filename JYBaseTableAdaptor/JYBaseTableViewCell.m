//
//  JYBaseTableViewCell.m
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/28/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#import "JYBaseTableViewCell.h"

@implementation JYBaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self addAndLayoutSubviews];
}

// 在该处，cell的大小还未确定
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addAndLayoutSubviews];
    }
    return self;
}

- (void)addAndLayoutSubviews {
    self.titleLab = [[UILabel alloc] init];
    [self.contentView addSubview:self.titleLab];
    
    self.contentV = [[UILabel alloc] init];
    [self.contentView addSubview:self.contentV];
    
    // 不在layoutSubviews中使用frame进行定位，否则自动布局约束可能不生效
    CGFloat contentViewW = self.frame.size.width;
    CGFloat contentViewH = self.frame.size.height;
    self.titleLab.frame = CGRectMake(20, 0, contentViewW / 3, contentViewH - 2);
    self.contentV.frame = CGRectMake(CGRectGetMaxX(self.titleLab.frame), 0,
                                     contentViewW - CGRectGetMaxX(self.titleLab.frame) - 20 - 20, contentViewH - 2);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setModel:(JYBaseCellModel *)model {
    if (_model != model) {
        _model = model;
    }
    
    CGRect tFrame = self.titleLab.frame;
    CGRect cFrame = self.contentV.frame;
    // titleLab变宽后，contentV向右移动避免重叠
    self.titleLab.text = _model.title;
    [self.titleLab sizeToFit];
    
    CGFloat offset = CGRectGetWidth(self.titleLab.bounds) - CGRectGetWidth(tFrame);
    tFrame.size.width += offset;
    self.titleLab.frame = tFrame;
    
    if (CGRectGetMaxX(self.titleLab.frame) > CGRectGetMinX(self.contentV.frame)) {
        cFrame.origin.x += offset;
        cFrame.size.width -= offset;
        self.contentV.frame = cFrame;
    }
    
    if ([self.contentV respondsToSelector:@selector(setText:)]) {
        ((UILabel *)self.contentV).text = _model.content;
        ((UILabel *)self.contentV).text = _model.content;
    }
    if ([self.contentV respondsToSelector:@selector(setTextAlignment:)]) {
        ((UILabel *)self.contentV).textAlignment = _model.contentAlignment;
    }
   
    if (_model.titleSizeFit) {

        self.titleLab.frame = CGRectMake(_model.edgeInsets.left, _model.edgeInsets.top,
                                         CGRectGetWidth(self.titleLab.bounds),
                                         CGRectGetHeight(self.frame) - _model.edgeInsets.top - _model.edgeInsets.bottom);
        
        self.contentV.frame = CGRectMake(CGRectGetMaxX(self.titleLab.frame), _model.edgeInsets.top,
                                         CGRectGetWidth(self.frame) - CGRectGetWidth(self.titleLab.bounds) - _model.edgeInsets.left - model.edgeInsets.right,
                                         CGRectGetHeight(self.frame) - _model.edgeInsets.top - _model.edgeInsets.bottom);
    }
}

- (void)setContentV:(UIView *)contentV {
    if (_contentV != contentV) {
        _contentV = contentV;
    }
    [self.contentView addSubview:_contentV];
}



@end
