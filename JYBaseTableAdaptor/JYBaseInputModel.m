//
//  JYBaseRegModel.m
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/4.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseInputModel.h"
#import "JYVerifyRegEx.h"

@implementation JYBaseInputModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.need = YES;
        self.changeable = YES;
    }
    return self;
}

+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title {
    
    JYBaseInputModel *model = [super modelWithItemKey:itemKey title:title];
    model.placeholder = [NSString stringWithFormat:@"请输入%@", title];
    return model;
}

/// 重写父类方法，否则部分属性初始化不成功
+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title content:(NSString *)content {
    JYBaseInputModel *model = [self modelWithItemKey:itemKey title:title];
    model.content = content;
    return model;
}

+ (instancetype)modelWithItemKey:(NSString *)itemKey title:(NSString *)title placeholder:(NSString *)contentPlaceholder {
    JYBaseInputModel *model = [self modelWithItemKey:itemKey title:title];
    model.placeholder = contentPlaceholder;
    return model;
}

- (void)setNeed:(BOOL)need {
    [super setNeed:need];
    
    if (need == NO) {
        self.content = self.content ? : @"";
        self.contentID = @(-NSIntegerMax);
    }
}

- (BOOL)verifyContentLength {
    if (self.isNeed
        && ((self.content.length < 1) && [self.contentID isEqualToNumber:@(-NSIntegerMax)])) {
        return NO;
    }
    return YES;
}

- (BOOL)isEqual:(JYBaseInputModel *)object {
    if ([self.itemKey isEqualToString:object.itemKey]) {
        return YES;
    }
    return [super isEqual:object];
}

- (BOOL)verifyContent {
    if (!self.isNeed) {
        return YES;
    }
    if ([self verifyContentLength] && self.regEx) {
        
        if ([self isKindOfClass:NSClassFromString(@"JYInfo_TFModel")]
            && [[self valueForKey:@"encrypt"] boolValue] ) { // 如果是加密控件的输入由控件自身进行校验
            return YES;
        }
        
        return [JYVerifyRegEx verifyInputString:self.content regEx:self.regEx];
    } else if ([self verifyContentLength] && !self.regEx) { // 如果长度符合，且无正则表达式直接返回yes
        return YES;
    } else {
        return NO;
    }
}

- (NSString *)debugDescription {
    NSString *description = [NSString stringWithFormat:@"<%p>, title:%@ itemKey:%@ contentID:%@ content:%@", self, self.title, self.itemKey, self.contentID, self.content];
    
    return description;
}

@end
