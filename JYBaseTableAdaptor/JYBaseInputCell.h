//
//  JYBaseRegTableViewCell.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/4.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import "JYBaseTableViewCell.h"
#import "JYBaseInputView.h"
#import "JYAdaptorInputViewProtocol.h"


/**
 2. 信息录入除了JYInfoInputTypeTextField，还有其他类型包括不限于：JYInfoInputTypePicker,JYInfoInputTypeRadioBtn，
    这些控件可以直接覆盖在UITextField上或者将UITextField替换成自定义的View；
 */
@interface JYBaseInputCell : JYBaseTableViewCell <JYAdaptorInputViewDelegate>

/**
 该属性为UIView的类型，默认封装了一个UITextField，子类可以进行覆盖
 */
@property (nonatomic, strong) UIView <JYAdaptorInputViewProtocol> *contentV;


@end
