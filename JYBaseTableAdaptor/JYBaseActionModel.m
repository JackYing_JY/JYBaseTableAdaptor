//
//  JYActionModel.m
//  YBSYBank-iPhone
//
//  Created by 应明顺 on 1/10/19.
//  Copyright © 2019 CSII. All rights reserved.
//

#import "JYBaseActionModel.h"

NSString * const kJYActionIDSelected = @"kJYActionIDSelected";
NSString * const kJYActionIDMore = @"kJYActionIDMore";
NSString * const kJYActionIDToggle = @"kJYActionIDToggle";

@implementation JYBaseActionModel


- (NSString *)description {
    
    NSString *des = [NSString stringWithFormat:@"<%@:%p:>, name:%@, actionID:%@, url:%@, content:%@", [self class], self, self.actionName, self.actionID, self.URLStr, self.content];
    return des;
}

@end
