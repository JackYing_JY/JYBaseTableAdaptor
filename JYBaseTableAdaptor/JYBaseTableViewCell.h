//
//  JYBaseTableViewCell.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/28/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYBaseCellModel.h"
#import "JYBaseActionModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef id _Nullable (^AccessoryAction)(JYBaseActionModel *action, JYBaseCellModel *cellModel);

/**
 1. cell的高度取决于内容的多少，当前样式的cell只有标题和信息录入控件，不包含其他内容，所以高度基本定为44的高度；
 3. 在设置了其他类型的输入控件时，cell的高度就需要重新设置；
 */
@interface JYBaseTableViewCell : UITableViewCell

@property (nonatomic, copy) JYBaseCellModel *model;
@property (nonatomic, strong) UILabel *titleLab;
/**
 该属性为UIView的类型，默认封装了一个UILabel，子类可以进行覆盖
 */
@property (nonatomic, strong) UIView *contentV;

@property (nonatomic, copy) AccessoryAction accessoryAction;

@end

NS_ASSUME_NONNULL_END
