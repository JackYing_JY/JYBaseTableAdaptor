//
//  JYInfoInputProtocol.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/5.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JYAdaptorInputViewProtocol;

@protocol JYAdaptorInputViewDelegate <NSObject>

/// 输入控件 本身 的事件
@optional
- (void)inputViewDidBeginInputView:(UIView<JYAdaptorInputViewProtocol> *)inputView;
- (void)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView didFinishInput:(id)content;
- (BOOL)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

/// 输入控件 附属控件 的事件
- (void)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView didBeginAccessoryAction:(NSString *)name;
- (void)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView didEndAccessoryAction:(NSString *)name;

@end



@protocol JYAdaptorInputViewProtocol <NSObject>

/**
 相关步骤的回调
 */
@property (nonatomic, weak) id<JYAdaptorInputViewDelegate> delegate;

@required
- (void)setContent:(id)content;

@optional
/// 添加多个代理
- (void)addDelegate:(id<JYAdaptorInputViewDelegate>)delegate;
- (void)setRegEx:(NSString *)RegEx;
- (void)setPlaceholder:(NSString *)placeholder;
- (void)setKeyboardType:(UIKeyboardType)keyboardType;
- (void)setEditable:(BOOL)editable;


@end
