//
//  JYVerifyRegEx.h
//  MYCCBRegisterSubAccount
//
//  Created by 应明顺 on 2018/7/4.
//  Copyright © 2018年 JackYing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// 仅针对以下格式的正则表达式，包含“^[]{}$”/"^[]*$"/"^[]+$"
@interface JYVerifyRegEx : NSObject

/// 提取正则中的简单限制字符，即[]内部的范围
+ (NSString *)simpleChartVerifyRegEx:(NSString *)regEx;

/// 提取正则中的字符长度限制，即{}中的内容
/// w:min, H:max
+ (CGSize)contentSizeOfRegEx:(NSString *)regEx;

/// 验证输入是否合法
+ (BOOL)verifyInputString:(NSString *)input regEx:(NSString *)regEx;


@end





// ------------------------------------------------------------------------------------------------------------------------------



/// 省份证号，^[0-9xX]{15,18}$
static NSString *const JYRegEx_IdNum = @"^[0-9xX]{15,18}$";
/// 手机号，^[0-9]{11,11}$
static NSString *const JYRegEx_Phone = @"^1[3,4,5,7,8,9][0-9][0-9]{8}|1[3,4,5,7,8,9][0-9][\\*]{4}[0-9]{4}|[0-9]{1,11}$";//@"^[0-9]{11}$";//@"^1[3,4,5,7,8,9][0-9][0-9]{8}|1[3,4,5,7,8,9][0-9][\\*]{4}[0-9]{4}$";
/// 交易密码，^[0-9]{6,6}$
static NSString *const JYRegEx_PasswordTrans = @"^[0-9]{6,6}$";
// 登录密码非纯字母/数字，(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]{6,16}
/// 登录密码，[0-9a-zA-Z]{6,16}
static NSString *const JYRegEx_PasswordLogin = @"^[0-9a-zA-Z]{6,16}$";
/// 银行卡号，^[0-9]{16,19}$
static NSString *const JYRegEx_BankCardNo = @"^[0-9]{16,19}$";// TODO:16或19
/// 验证码，^[0-9]{6,6}$
static NSString *const JYRegEx_MsgCode = @"^[0-9]{6,6}$";
/// 图形验证码，^[0-9]{6,6}$
static NSString *const JYRegEx_ImgCode = @"^[0-9a-zA-Z]{4}|[0-9a-zA-Z]{0,4}$";
/// 用户名，^[A-Za-z0-9<>\\s\\u005b\\u005d{}()\\u4E00-\\u9FBB\\u3400-\\u4DBF\\uF900-\\uFAD9\\u3000-\\u303F\\u2000-\\u206F\\uFF00-\\uFFEF-]{1,36}$
static NSString *const JYRegEx_AccoutName = @"^[a-zA-Z0-9<>\\s\\u005b\\u005d{}()\\u4E00-\\u9FBB\\u3400-\\u4DBF\\uF900-\\uFAD9\\u3000-\\u303F\\u2000-\\u206F\\uFF00-\\uFFEF-]{1,36}$";
/// 登录名，^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{4,14}$
static NSString *const JYRegEx_LoginID = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{4,14}$";
/// 预留信息，中文+字母+数字，^[0-9a-zA-Z\u4e00-\u9fa5➋➌➍➎➏➐➑➒]{4,14}$
static NSString *const JYRegEx_ChinesASCII = @"^[0-9a-zA-Z\u4e00-\u9fa5➋➌➍➎➏➐➑➒]{4,14}$";
/// 数字字母组合，^[0-9a-zA-Z]*$
static NSString *const JYRegEx_ASCIIChar = @"^[0-9a-zA-Z]*$";
