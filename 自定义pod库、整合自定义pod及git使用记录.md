## 自定义Pod库

> 参考文档：
>
> [自定义podspec， cocoapods助力组件化](https://www.jianshu.com/p/07c1693d40e4) --》podspec配置了解
>
> [如何发布自己的开源框架到CocoaPods](https://blog.csdn.net/blog_t/article/details/59570385) --》 podspec发布上线
>
> [利用 podspec 的 subspec 来实现多个预处理宏的灵活配置](https://0error0warning.com/blog/14797354347923.html) --》 subspec了解
>
> [iOS CocoaPods组件平滑二进制化解决方案](https://www.jianshu.com/p/5338bc626eaf) --》 subspec了解
>
> [iOS CocoaPods组件平滑二进制化解决方案及详细教程二之subspecs篇](https://www.jianshu.com/p/85c97dc9ab83) --》 subspec了解
>
> [iOS组件化（三）-podspec使用详解](https://blog.csdn.net/zramals/article/details/81388703) --》依赖多模块添加
>
> [Cocopods组件化之podspec文件解析](https://www.jianshu.com/p/8ba2de69de5f)--》文件夹多级(A>>B>>C)分组



### 大致步骤

1. 创建待共享的库项目，使用Gitee做远端仓库；

   * 本地创建共享项目，使用Git指令创建本地仓库，添加所有本地文件到本地仓库，将代码提交到本地库；
   * 在Git上创建项目，获取Git下载地址；
   * 使用Git指令，在本地目录下降Git远程仓库引用到本地，拉取远端代码到本地，切换到对应的分支，推送本地修改到远端仓库；

2. 创建`xxx.podspec`文件；

   使用指令：`pod spec create JYBaseTableAdaptor`，创建出 `JYBaseTableAdaptor.podspec`文件；

3. 修改podspec文件内容；

   * 使用Ruby

   * 基础配置：项目名、简介、描述、协议类型、主页、作者信息
   * 特别注意：针对单独的平台、**源地址、源文件**
   * 源地址：`s.source = { :git => "https://gitee.com/xxx.git" }` 
   * 源文件：`s.source_files  = "JYBaseTableAdaptor/**/*.{h,m}"`
   * **源地址和源文件，合并在一起需要是一个完整的路径，即所指向的地址下能找到源文件夹及文件；**

4. 使用指令对本地的podspec进行验证；

   指令：`pod spec lint JYBaseTableAdaptor.podspec`

   根据提示错误修改相应的配置，忽略警告的选项`--allow-warnings`；

   不指定平台可能报**非指定的其他平台**UIKit组件找不到的错；

5. 上传podspec到远端仓库；

   将`xxx.podspec`上传到`.Git` 根目录；

6. 自定义分组：

   1. 预设部分分组；
   2. 给个分组添加依赖；

7. 发布podspec：

   1. 指令：`pod trunk push JYBaseTableAdaptor.podspec`，

   2. 如提示：

   ```ruby
   [!] You need to register a session first.
   ```

   则使用`pod trunk register 邮箱地址 "用户名" --description="xxx"`指令创建一个`trunk`，完成后邮箱收到激活链接，激活后再次提交；

   3. 验证：通过`pod search xxx`，进行搜索，发布成功则能搜索到。

8. podspec更新

   1. 使用指令`pod trunk push JYBaseTableAdaptor.podspec`，和发布时的指令相同；



### 遇到的坑

1. 一定要用`pod spec lint xxx.podspec`对podspec文件进行正确性检查，否则尝试集成使用千百回也不知哪里错；
2. 忽略警告的指令：`pod spec lint xxx.podspec --allow-warnings`；
3. 需要将`xxx.podspec`与`Classes`文件夹放在**同一级目录下**；
4. 需要将`xxx.podspec`上传到`.Git`所指向的第一级目录，也就是访问`xxx.git`时可以直接看到`xxx.podspec`，否则在Podfile中添加了`xxx.git`URL后cocoapod不能找到源；
5. `s.source_files`的内容确定目录结构；
6. 参考MJRefresh的podspec以及他在Git上的项目结构；
7. 多个依赖时，使用换行进行标识；
8. 配置`ss.source_files`时使用正则标识进行匹配本地路劲；
9. 配置`ss.source_files`时，因为podspec和和目标分组集合文件夹在同一级目录，所以需要在目录下寻找文件；
10. 验证时需要对git进行发布，同podsepc中版本必须是一致的；
11. 发布时，需要给s.source添加tag，否则lint时验证不通过，不能进行发布；
12. 更新时podspec中的版本号需要增加，git上对应的tag需要时发布版本的tag，不能是预览版；



## 集成自定义Pod库

### 大致步骤

1. 选择集成途径；

   在没有提交到podspec仓库，可以选择Git路径或者本地代码库；

   如果提交到spec仓库，并审核通过，可以使用正常的方式来引用；

2. 生成Podfile文件；

   指令：`pod init`

3. 集成pod库内容；

   `pod 'JYBaseTableAdaptor', :git => 'https://gitee.com/xxxx.git'`



### 遇到的坑 

1. 在Podfile中使用`xxx.git`方式来集成时，需要将URL直接指向含有podspec文件的目录；
2. Podfile中的源地址配置的写法类似podspec的配置，一个可以添加模块名、一个不能加；





# Git使用笔记

1. 参考：
   * [src refspec xxx does not match any 错误处理办法](https://blog.csdn.net/zhu1500527791/article/details/53119579)；---> 分支切换
   * [创建版本库](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000/0013743256916071d599b3aed534aaab22a0db6c4e07fd0000) ---> 删除、移动
   * [iOS组件化（三）-podspec使用详解](https://blog.csdn.net/zramals/article/details/81388703) ---> 添加多个依赖
2. 要点：
   1. 创建本地Git库：`git init`;

   2. 添加本地代码到本地库：`git add .`；

   3. 将本地与远端库进行挂钩：`git remote add origin xxx.git`；

   4. 查看当前git配置：`git config --list`；分系统级别、用户级别、当前仓库级别；

   5. 分支切换：`git checkout -b xxx`；

   6. 拉取远端仓库代码到本地：`git pull -r origin branche `；

   7. 提交本地代码：`git commit -m "xxxx"`；

   8. 推送到远端仓库：`git push -u origin branche`；

   9. 删除本地库文件：`git rm  -r xxxx`；

   10. 查看提交日志：`git log`；

   11. 打开当前文件夹：`open .`；

   12. 查看当前git的跟踪状态：`git status`;

   13. 修改文件内容后，git标识旧文件delete，新文件标识modified，使用删除和添加指令完成修改：`git rm xxx `和`git add xxx`；

   14. 多文件控制：`git commit -a`，打开vim，编辑退出完成本地库提交，在vim中对对应的变动添加日志；在所有变动前添加标注，对所有变动生效，也可以对单个文件进行标注；

   15. 打标签：`git -a tag vx.x.x -m "标签说明"`，`-a`：a 表示需要添加标注，`-m`：表示紧跟的标注内容，不加`-m`pod会自动打开vi进行编辑；不加`-m`时可能在tag上传时，标注不能同步上传；

   16. 分享标签：`git push origin x.x.x.x`，同时上传tag和对应的标注；

   17. 分支推送后造成冲突需要手动合并：

       1. 手动合并此 Pull Request

          1. 切换到分支：`$ git checkout master`

          2. 拉取请求合并分支：`$ git pull https://gitee.com/xxx.git develop`

          3. 解决后推送远端主分支：`$ git push origin master`

       2. 将新分支拉取到本地，修复冲突：

          1. 拉取主分支代码至本地：`git pull -r origin master`

          2. 查看冲突文件：`git am --show-current-patch`，

             ```shell
             Use 'git am --show-current-patch' to see the failed patch
             ```

          3. 重置冲突标志：`git add/rm <conflicted_files>`、`git rebase --continue`，

             ```shell
             Resolve all conflicts manually, mark them as resolved with
             "git add/rm <conflicted_files>", then run "git rebase --continue".
             You can instead skip this commit: run "git rebase --skip".
             To abort and get back to the state before "git rebase", run "git rebase --abort".
             ```

   18. 配置仓库用户名和邮箱

       1. 配置全局的用户名和邮箱，命令分别为

          `git config --global user.name "username"`

          `git config --global user.email "email"`

       2. 查看全局的用户名和邮箱，命令分别为

          `git config --global user.name`

          `git config --global user.email` 

       3. 为单一的仓库配置用户名和邮箱，命令分别为

          `git config user.name "username"`

          `git config user.email "email"`

       4. 查看单一的仓库配置的用户名和邮箱，命令分别为

          `git config user.name`

          `git config user.email` 


