//
//  ViewController.m
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 11/20/18.
//  Copyright © 2018 JackYing. All rights reserved.
//

#import "ViewController.h"
#import "JYTableAdaptor.h"
#import "JYTableBaseAdaptor+EditDecorate.h"
#import "PlusToggleModel.h"
#import "PlusToggleCell.h"
#import "NetworkRefreshCell.h"
#import "FlexibleInputCell.h"
#import "CustomTableViewCell.h"

@interface ViewController () {
    NSArray *previousDataSource;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.topMargin = 16;
    
    [self configTableAdaptor];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    // cell左滑按钮选择项适配图片（上下方向）
    [self.adaptor configSwipeButtons];
}

- (void)configTableAdaptor {
    
    self.adaptor.title = @"演示";
    self.adaptor.key = @"demo";

    [self.adaptor regsiterCell:[JYBaseInputCell class] forModelClass:[JYBaseInputModel class]];
    [self.adaptor regsiterCell:[FlexibleInputCell class] forModelClass:[FlexibleInputModel class]];
    [self.adaptor regsiterCellNib:[PlusToggleCell class] forModelClass:[PlusToggleModel class]];
    [self.adaptor regsiterCellNib:[NetworkRefreshCell class] forModelClass:[NetworkRefreshModel class]];
    [self.adaptor regsiterCell:[JYBaseInputCell class] forModelClass:[FlexibleInputModel class] extid:@"mutable"];
    [self.adaptor regsiterCell:[CustomTableViewCell class] forModelClass:[CustomCellModel class]];
    
    self.adaptor.cellEditIcon = @[[UIImage imageNamed:@"doubt"], [UIImage imageNamed:@"pic"]];
    [self.adaptor setCellEditable:^BOOL(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model) {
            return YES;
    }];


    [super configTableAdaptor];
}

- (NSArray *)configTableViewDataSource {
    
    JYBaseCellModel *name = [JYBaseCellModel modelWithItemKey:@"name" title:@"姓名" content:@"JackYing"];
    name.estimatedHeight = 150;
    JYBaseInputModel *emil = [JYBaseInputModel modelWithItemKey:@"emil" title:@"emil" content:@"jk7896@126.com"];
    JYBaseInputModel *msgCode = [JYBaseInputModel modelWithItemKey:@"msgCode" title:@"验证码" placeholder:@"请输入您收到的验证码"];
    
    FlexibleInputModel *flexibleModel = [FlexibleInputModel modelWithItemKey:@"灵活长度" title:@"多级代理" placeholder:@"输入框添加多级代理"];
    FlexibleInputModel *intricateModel = [FlexibleInputModel modelWithItemKey:@"mutable" title:@"多对一" placeholder:@"多model对一cell，需要注意两者的对应关系"];
    intricateModel.extid = @"mutable";
    
    CustomCellModel *customModel = [CustomCellModel modelWithItemKey:@"CustomCell" title:@"多内容"];
    
    JYBaseGroupModel *group1 = [JYBaseGroupModel groupModelWithRowModelArr:@[name, emil, msgCode, flexibleModel, intricateModel, customModel]];
    group1.headerModel = [JYBaseHeaderModel modelHeaderWithTitle:@"组头-快速注册" subTitle:@"请录入正确的手机号"];
    group1.footerModel = [JYBaseFooterModel modelFooterWithTitle:@"组尾-下一步"];
    group1.footerModel.backgroundColor = [UIColor redColor];
    
    PlusToggleModel *plusToggle = [PlusToggleModel modelWithItemKey:@"plus" title:@"事件绑定，动态更新"];
    
    JYBaseGroupModel *group2 = [JYBaseGroupModel groupModelWithRowModelArr:@[plusToggle]];
    group2.headerModel = [JYBaseHeaderModel modelHeaderWithTitle:@"组头-绑定事件" subTitle:@"视图随model动态更新内容"];
    group2.footerModel = [JYBaseFooterModel modelFooterWithTitle:@"组尾-自定义颜色"];
    group2.footerModel.backgroundColor = [UIColor cyanColor];
    
    NetworkRefreshModel *network = [NetworkRefreshModel modelWithItemKey:@"Network" title:@"2秒页面重新布局"];
    JYBaseGroupModel *group3 = [JYBaseGroupModel groupModelWithRowModelArr:@[network]];
    
    return @[group1, group2, group3];
}

- (void)configAdaptorAction {
    
    __weak typeof(self) weakSelf = self;
    self.adaptor.nextAction = ^(JYBaseHeaderFooterModel *footerModel, id action) {
        if ([action isEqualToString:@"next"]) {
            __strong typeof(weakSelf) self = weakSelf;
            NSLog(@"点击了第二步");
            [self nextAction:footerModel];
        }
    };
    
    self.adaptor.cellSelectedAction = ^(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model) {
            NSLog(@"点击cell ==> indexPath:%@\nmodel:%@", indexPath, model);
    };
    
    [self.adaptor setCellAccessoryAction:^id(JYBaseActionModel *action, JYBaseCellModel *cellModel) {
        __strong typeof(weakSelf) self = weakSelf;
        if ([action.actionID isEqualToString:kJYActionIDToggle]) {
            if ([action.actionName isEqualToString:@"Network"]) {
                [self refreshDataSource];
            } else if ([action.actionName isEqualToString:@"Reset"]) {
                [self resetDataSource];
            } else if ([action.actionName isEqualToString:@"refreshSection"]) {
                [self refreshSection];
            }
        }
        return nil;
    }];
    
    [self.adaptor setCellEditAction:[self tableViewCellEditAction]];
}

- (void)nextAction:(JYBaseHeaderFooterModel *)footerModel {
    [super nextAction:footerModel];
    
    if ([self verifyDataSourceContent]) {
        
        
    }
}


#pragma mark - cellEditAction
- (CellEditAction)tableViewCellEditAction {
    return ^NSArray<UITableViewRowAction *> * _Nullable(NSIndexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model) {
        
        UITableViewRowAction *unbind = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleNormal) title:@"编辑" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            NSLog(@"点击%@", action.title);
        }];
        
        UITableViewRowAction *edit = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleNormal) title:@"咨询" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            NSLog(@"点击%@", action.title);
        }];
        
        
        return @[edit, unbind];
    };
}

- (void)refreshDataSource {
    
    previousDataSource = [self.adaptor.dataArr copy];
    
    JYBaseCellModel *date = [JYBaseCellModel modelWithItemKey:@"date" title:@"日期" content:@"2019.06.23"];
    date.estimatedHeight = 150;
    JYBaseCellModel *location = [JYBaseCellModel modelWithItemKey:@"location" title:@"地点" content:@"重庆"];
    JYBaseCellModel *weather = [JYBaseInputModel modelWithItemKey:@"weather" title:@"天气" placeholder:@"阴"];
    
    JYBaseGroupModel *groupNew = [JYBaseGroupModel groupModelWithRowModelArr:@[date, location, weather]];
    
    
    NetworkRefreshModel *network = [NetworkRefreshModel modelWithItemKey:@"Reset" title:@"2秒页面恢复"];
    JYBaseGroupModel *group = [JYBaseGroupModel groupModelWithRowModelArr:@[network]];
    
    [self.adaptor deleteAllGroup];
    [self.adaptor appendGroupList:@[groupNew, group]];
}

- (void)resetDataSource {
    [self.adaptor  deleteAllGroup];
    [self.adaptor appendGroupList:previousDataSource];
}

- (void)refreshSection {
    JYBaseHeaderModel *headerModel0 = [self.adaptor groupAtIndex:0].headerModel;
    headerModel0.subTitle = [NSString stringWithFormat:@"%@\n%@", headerModel0.subTitle, @"视图随model动态更新内容"];
    headerModel0.estimatedHeight += 22;
    
    //JYBaseHeaderModel *headerModel1 = [self.adaptor groupAtIndex:1].headerModel;
    //headerModel1.title = @"请录入正确的手机号";
    
    // 仅对指定section进行更新
    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:0];
    [self.tableView reloadSections:set withRowAnimation:(UITableViewRowAnimationNone)];
}

@end
