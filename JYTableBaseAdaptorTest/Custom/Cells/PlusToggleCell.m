//
//  PlusToggleCell.m
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 3/26/19.
//  Copyright © 2019 JackYing. All rights reserved.
//

#import "PlusToggleCell.h"

@interface PlusToggleCell ()



@end

@implementation PlusToggleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setModel:(JYBaseCellModel *)model {
    [super setModel:model];
}

- (IBAction)plus:(id)sender {
    NSInteger ID = [self.model.contentID integerValue];
    ID++;
    self.model.contentID = @(ID);
}

- (IBAction)minus:(id)sender {
    NSInteger ID = [self.model.contentID integerValue];
    ID--;
    self.model.contentID = @(ID);
}

@end
