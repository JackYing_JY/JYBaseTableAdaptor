//
//  InputTableViewCell.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 3/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "JYBaseInputCell.h"
#import "FlexibleInputModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FlexibleInputCell : JYBaseInputCell

@end

NS_ASSUME_NONNULL_END
