//
//  CustomTableViewCell.m
//  JYTableBaseAdaptorTest
//
//  Created by MingShun on 9/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self placeSubviews];
    }
    return self;
}

- (void)placeSubviews {
    
    UIButton *refreshSectionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshSectionBtn setTitle:@"section header refresh" forState:(UIControlStateNormal)];
    [refreshSectionBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [refreshSectionBtn addTarget:self action:@selector(refreshSection:) forControlEvents:(UIControlEventTouchUpInside)];
    [refreshSectionBtn setFrame:CGRectMake(16, 16, 200, 30)];
    [refreshSectionBtn setBackgroundColor:[UIColor systemGrayColor]];
    [refreshSectionBtn setShowsTouchWhenHighlighted:YES];
    [self.contentView addSubview:refreshSectionBtn];
    
}

- (void)refreshSection:(UIButton *)sender {
    
    if (self.accessoryAction) {
        
        JYBaseActionModel *action = [[JYBaseActionModel alloc] init];
        action.actionID = kJYActionIDToggle;
        action.actionName = @"refreshSection";
        
        self.accessoryAction(action, self.model);
    }
}

@end
