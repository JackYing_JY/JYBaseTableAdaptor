//
//  NetworkRefreshCell.m
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 6/23/19.
//  Copyright © 2019 JackYing. All rights reserved.
//

#import "NetworkRefreshCell.h"

@implementation NetworkRefreshCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)toggleNetwork:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.accessoryAction) {
            JYBaseActionModel *action = [JYBaseActionModel new];
            action.content = self.model;
            action.actionName = self.model.itemKey;
            action.actionID = kJYActionIDToggle;
            self.accessoryAction(action, self.model);
        }
    });
}

@end
