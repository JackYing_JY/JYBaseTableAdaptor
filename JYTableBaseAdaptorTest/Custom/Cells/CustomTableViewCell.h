//
//  CustomTableViewCell.h
//  JYTableBaseAdaptorTest
//
//  Created by MingShun on 9/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "JYBaseTableViewCell.h"
#import "CustomCellModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomTableViewCell : JYBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
