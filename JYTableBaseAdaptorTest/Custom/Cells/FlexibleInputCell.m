//
//  InputTableViewCell.m
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 3/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "FlexibleInputCell.h"



/// 输入可控长度
@implementation FlexibleInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentV addDelegate:self];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

//    [self.contentV addDelegate:[[FlexibleInputCellInner alloc] init]];
    // Configure the view for the selected state
}

#pragma mark - JYAdaptorInputViewDelegate
- (void)inputViewDidBeginInputView:(UIView<JYAdaptorInputViewProtocol> *)inputView {
    
}

- (void)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView didFinishInput:(id)content {
    self.model.content = content;
}

- (BOOL)inputView:(UIView<JYAdaptorInputViewProtocol> *)inputView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

@end




