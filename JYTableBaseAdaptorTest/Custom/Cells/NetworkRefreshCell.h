/*! 
  @header NetworkRefreshCell.h

  @author Created by 应明顺 on 6/23/19.

  @version 1.0 6/23/19 Creation

Copyright © 2019 JackYing. All rights reserved.
 */

#import "JYBaseTableViewCell.h"
#import "NetworkRefreshModel.h"

/*!
  @class NetworkRefreshCell

  @abstract 模拟网络请求
 */

NS_ASSUME_NONNULL_BEGIN

@interface NetworkRefreshCell : JYBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
