//
//  InputTableViewModel.h
//  JYTableBaseAdaptorTest
//
//  Created by 应明顺 on 3/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "JYBaseInputModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FlexibleInputModel : JYBaseInputModel

@end

NS_ASSUME_NONNULL_END
