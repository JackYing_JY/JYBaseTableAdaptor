/*! 
  @header NetworkRefreshModel.h

  @author Created by 应明顺 on 6/23/19.

  @version <#version#> 6/23/19 Creation

Copyright © 2019 JackYing. All rights reserved.
 */

#import "JYBaseCellModel.h"

/*!
  @class NetworkRefreshModel

  @abstract <#description#>
 */

NS_ASSUME_NONNULL_BEGIN

@interface NetworkRefreshModel : JYBaseCellModel

@end

NS_ASSUME_NONNULL_END
