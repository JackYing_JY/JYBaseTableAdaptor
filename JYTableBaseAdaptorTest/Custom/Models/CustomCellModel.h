//
//  CustomCellModel.h
//  JYTableBaseAdaptorTest
//
//  Created by MingShun on 9/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "JYBaseCellModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomCellModel : JYBaseCellModel

@end

NS_ASSUME_NONNULL_END
