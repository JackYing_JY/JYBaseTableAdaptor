//
//  CustomCellModel.m
//  JYTableBaseAdaptorTest
//
//  Created by MingShun on 9/17/20.
//  Copyright © 2020 JackYing. All rights reserved.
//

#import "CustomCellModel.h"

@implementation CustomCellModel

- (CGFloat)estimatedHeight {
    return 120;
}

@end
