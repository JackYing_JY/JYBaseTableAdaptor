# JYBaseTableAdaptor

#### 项目介绍
写一个简单的表格适配器，需要持有优化。

1. 利用指针特性，在VC、Adaptor、Cell直接进行传递model的指针，在cell中通过指针对model的具体进行修改，在VC中可以拿到更改后的值。因此具有**高度耦合**性。

2. 通过block回调形式，具有**高内聚**特性，只需配置相应接口即可实现将消息外传功能；
3. 使用**策略模式**，实现"model - cell" 一对一对应关系。使用`-regsiterCell:forModelClass:`或者使用`-regsiterCellNib:forModelClass:`方法即可实现model与cell的绑定，后只需在`-configTableViewDataSource:`方法中对相关的model进行实例并加入分组数列中即可实现数据展示；
4. cell高度动态根据model中`estimatedHeight`属性进行调整；
5. 网络请求后只需根据请求结果设置model的`contentUpdated`，再使用`indexPathForModel:`对model所在indexPath进行查找，成功后对tableview的指定indexPath进行刷新即可更新数据的展示；
6. 因为使用了`-reloadRowsAtIndexPaths:withRowAnimation:`方法进行cell更新，可能导致cell再次创建，旧cell被移除显示池`visibleCells`；
7. 特点：动态绑定注册cell、一个model对应一个cell、方便添加表头表尾视图、动态更新model输入内容、动态计算cell高度、


#### 安装教程

1. 使用cocoapod进行安装，`pod 'JYBaseTableAdaptor'`直接安装；

#### 使用说明

1. 继承`JYTableViewController`，可以使用内置tableview，以及其他已有功能；
2. 每个VC的adaptor可以进行自定义，需要继承`JYTableAdaptor`，在`JYTableAdaptor`内注册cell相应的`model`即可将`model`与`cell`进行特定的绑定；
3. 在VC中`configVMAction`方法配置adaptor的行为，adaptor在cell发出相应的行为后，会将具体信息传递出来，以供操作；
4. model中指定相应属性，以及如何验证一个model的数据是否正确的方法；
5. cell中默认使用UItextfield做输入内容控件，可以继承后自定义；
6. adaptor使用分组模式，在整理DataSource时需要按`组-元素`的结构进行；
7. 提供普通的表头、表尾、分组头、分组尾的model、view；
8. 使用时需要对`JYTableViewController`、`JYTableAdaptor`，`JYBaseInputCell`、`JYBaseCellModel`进行继承；
9. 使用左滑功能时，在`viewDidLayoutSubviews`中调用`[self.adaptor configSwipeButtons]`方法即可，编辑图标直接使用`self.adaptor.cellEditIcon = @[[UIImage imageNamed:@"xxx"], [UIImage imageNamed:@"xxx"]];`即可；
10. 需要自定义更多tableview的delegate可以子类继承JYBaseTableAdaptor并直接实现UITabelViewDelegate协议即可；





## 使用

> 更详细的使用方式参考功能中ViewController.m部分；



1. 添加tableview
   1. 在`viewDidLoad`中调用`UIViewController+JYTabelAdaptor`分类方法`-[ViewController configTableAdaptor]`方法，进行TableView和adaptor的懒加载；
   2. 在`configTableAdaptor`前后对当前VC进行定制配置；
   3. tableview可以在当前VC中进行重新指定自定义tableview。在VC的延展中申明_tableView成员变量、tableview属性，在实现中使用`@synthesize tableView = _tableView;`进行同步覆盖；

1. 指定adaptor
   1. 在当前VC中申明指定的类型的adaptor属性，使用覆盖默认的adaptor；
   2. 在指定的类型的adaptor中注册cell和对应的model，如果需要进一步实现tableview的delegate事件，可在该adaptor中复写tableview的delegate方法；

   > 可以不指定其他adaptor直接使用默认的。此时需要在configTableAdaptor方法中注册cell和model，注册完成后再调用默认的configTableAdaptor方法完成配置操作；

   ```objc
   - (void)viewDidLoad {
       [super viewDidLoad];
       [self configTableAdaptor];
   }
   - (void)configTableAdaptor {
       
       self.adaptor.title = @"详情";
       self.adaptor.key = @"TravelDetailData";
       [self.adaptor regsiterCell:[xxxxCell class] forModelClass:[xxxxModel class]];
       
       [super configTableAdaptor];
   }
   ```


1. 添加数据源
   1. 当前adaptor仅将数据源视为`section-row`形式的数据结构进行解析；
   2. 设置数据源时数组内容按`(NSArray<JYBaseGroupModel *> *)`数据结构；
   3. 目前adaptor数据源中的JYBaseGroupModel对应section，即一个GroupModel生成一个section；
   4. UIViewController+JYTabelAdaptor默认将UITableView style设置为UITableViewStyleGrouped，但还未实现UITableViewStyleGrouped相关的特性，在自定义时可以设置为UITableViewStylePlain，两者的外观特性基本一致；

1. 配置adaptor事件
   1. adaptor事件包括cell的自身点击事件（cellSelectedAction）和cell中的附属事件	（cellAccessoryAction）；
   2. cellSelectedAction会默认将indexPath和对应的model传出；cellAccessoryAction默认传出model和与action相关的字典；其中action字典在自定义cell时进行指定；

1. 增加附属动作
   1. 另外目前adaptor还支持CellEditAction，用于编辑tableview时cell右侧的动作事件，默认支持2个事件。
   2. 编辑时右侧视图可以进行自定义，当前布局为<u>上图下字</u>的结构，通过cellEditIcon属性进行图片设置，文字通过UITableViewRowAction设置，保证两者对应，否则可能出问题。
   3. 该处使用系统默认的方式进行事件处理，需要传入`(NSArray<UITableViewRowAction *> *)`事件组；

```objective-c
- (xxxAdaptor *)adaptor {
    if (!_adaptor) {
        _adaptor = [xxxAdaptor tableAdaptorWithTableView:self.tableView title:@"设备管理" key:@"deviceManager"];
        _adaptor.dataArr = self.dataArr;
        _adaptor.cellEditIcon = @[[UIImage imageNamed:@"gps_mark"], [UIImage imageNamed:@"camera"]];
        _adaptor.cellEditAction = [self cellEditAction];
        _adaptor.cellEditable = ^BOOL(NSindexPath *indexPath, JYTableBaseAdaptor *adaptor) {
            return indexPath.section == 0;
        };
    }
    return _adaptor;
}
- (CellEditAction)cellEditAction {
    
    return ^NSArray<UITableViewRowAction *> * _Nullable(NSindexPath *indexPath, JYTableBaseAdaptor *adaptor, JYBaseCellModel *model) {
        
        UITableViewRowAction *unbind = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleNormal) title:@"解绑" handler:^(UITableViewRowAction * _Nonnull action, NSindexPath * _Nonnull indexPath) {
            DLog(@"点击%@", action.title);
        }];
        
        UITableViewRowAction *edit = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleNormal) title:@"编辑" handler:^(UITableViewRowAction * _Nonnull action, NSindexPath * _Nonnull indexPath) {
            DLog(@"点击%@", action.title);
        }];
        
        return @[edit, unbind];
    };
}

```



